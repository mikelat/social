# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151003160616) do

  create_table "brute_forces", force: :cascade do |t|
    t.string   "ip_address"
    t.integer  "score",        default: 0, null: false
    t.integer  "hidden_score", default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "brute_forces", ["ip_address"], name: "index_brute_forces_on_ip_address"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.string   "path"
    t.string   "path_indexed"
    t.integer  "category_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "categories", ["category_id"], name: "index_categories_on_category_id"
  add_index "categories", ["path_indexed"], name: "index_categories_on_path_indexed"

  create_table "comments", force: :cascade do |t|
    t.text     "text"
    t.text     "text_cached"
    t.string   "ip_address"
    t.integer  "depth",       default: 0
    t.decimal  "score_up",    default: 0.0
    t.decimal  "score_down",  default: 0.0
    t.boolean  "deleted",     default: false
    t.integer  "comment_id"
    t.integer  "link_id"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "comments", ["comment_id"], name: "index_comments_on_comment_id"
  add_index "comments", ["ip_address"], name: "index_comments_on_ip_address"
  add_index "comments", ["link_id"], name: "index_comments_on_link_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "links", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.string   "url"
    t.string   "domain"
    t.string   "ip_address"
    t.text     "text"
    t.boolean  "nsfw"
    t.boolean  "has_thumbnail",    default: false
    t.boolean  "thumbnail_failed", default: false
    t.integer  "comments_count",   default: 0
    t.integer  "content_type",     default: 0
    t.decimal  "score_up",         default: 0.0
    t.decimal  "score_down",       default: 0.0
    t.integer  "category_id"
    t.integer  "user_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "links", ["category_id"], name: "index_links_on_category_id"
  add_index "links", ["ip_address"], name: "index_links_on_ip_address"
  add_index "links", ["slug"], name: "index_links_on_slug"
  add_index "links", ["url"], name: "index_links_on_url"
  add_index "links", ["user_id"], name: "index_links_on_user_id"

  create_table "user_auths", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "auth_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_auths", ["auth_key"], name: "index_user_auths_on_auth_key"
  add_index "user_auths", ["user_id"], name: "index_user_auths_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "name_lower"
    t.string   "password_digest"
    t.string   "email"
    t.string   "ip_address"
    t.text     "profile"
    t.boolean  "admin"
    t.boolean  "show_nsfw"
    t.decimal  "karma"
    t.decimal  "weight_up",       default: 1.0
    t.decimal  "weight_down",     default: 0.0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "users", ["email"], name: "index_users_on_email"
  add_index "users", ["ip_address"], name: "index_users_on_ip_address"
  add_index "users", ["name_lower"], name: "index_users_on_name_lower"

  create_table "votes", force: :cascade do |t|
    t.decimal  "weight_up"
    t.decimal  "weight_down"
    t.decimal  "change"
    t.string   "referer"
    t.string   "ip_address"
    t.integer  "comment_id"
    t.integer  "link_id"
    t.integer  "user_id"
    t.datetime "last_action_at"
    t.datetime "created_at"
  end

  add_index "votes", ["comment_id"], name: "index_votes_on_comment_id"
  add_index "votes", ["ip_address"], name: "index_votes_on_ip_address"
  add_index "votes", ["link_id"], name: "index_votes_on_link_id"
  add_index "votes", ["user_id"], name: "index_votes_on_user_id"

end
