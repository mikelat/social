class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :text
      t.text :text_cached
      t.string :ip_address, index: true
      t.integer :depth, default: 0
      t.decimal :score_up, default: 0
      t.decimal :score_down, default: 0
      t.boolean :deleted, default: 0
      t.references :comment, index: true
      t.references :link, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
  end
end
