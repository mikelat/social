class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :path
      t.string :path_indexed, index: true
      t.references :category, index: true

      t.timestamps null: false
    end
  end
end
