class CreateUserAuths < ActiveRecord::Migration
  def change
    create_table :user_auths do |t|
      t.references :user, index: true
      t.string :auth_key, index: true

      t.timestamps null: false
    end
  end
end
