class CreateBruteForces < ActiveRecord::Migration
  def change
    create_table :brute_forces do |t|
      t.string :ip_address, index: true
      t.integer :score, null: false, default: 0
      t.integer :hidden_score, null: false, default: 0

      t.timestamps null: false
    end
  end
end
