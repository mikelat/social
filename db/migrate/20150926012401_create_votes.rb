class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.decimal :weight_up
      t.decimal :weight_down
      t.decimal :change
      t.string :referer
      t.string :ip_address, index: true
      t.references :comment, index: true
      t.references :link, index: true
      t.references :user, index: true

      t.timestamp :last_action_at

      t.datetime :created_at
      #t.timestamps null: false
    end
  end
end
