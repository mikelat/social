class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :name_lower, index: true
      t.string :password_digest
      t.string :email, index: true
      t.string :ip_address, index: true
      t.text :profile
      t.boolean :admin
      t.boolean :show_nsfw
      t.decimal :karma
      t.decimal :weight_up, default: 1
      t.decimal :weight_down, default: 0

      t.timestamps null: false
    end
  end
end
