class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :title
      t.string :slug, index: true
      t.string :url, index: true
      t.string :domain
      t.string :ip_address, index: true
      t.text :text
      t.boolean :nsfw
      t.boolean :has_thumbnail, default: 0
      t.boolean :thumbnail_failed, default: 0
      t.integer :comments_count, default: 0
      t.integer :content_type, default: 0
      t.decimal :score_up, default: 0
      t.decimal :score_down, default: 0
      t.references :category, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
  end
end
