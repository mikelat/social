require 'test_helper'

class LinkTest < ActiveSupport::TestCase
  def setup
    @main_category = Category.create! name: 'Main Category'
    @sub_category = Category.create! name: 'Sub Category',
                                     category_id: @main_category.id

    @link_url = Link.new title: 'Check out this example link!',
                         ip_address: '127.0.0.1',
                         url: 'http://www.google.com',
                         category_id: @sub_category.id
    @link_text = Link.new title: 'Text post',
                          ip_address: '127.0.0.1',
                          text: 'Example text',
                          category_id: @sub_category.id
  end

  test 'should be valid' do
    assert @link_url.valid?
    assert @link_text.valid?
  end

  test 'should not be allowed in main category' do
    @link_url.category_id = @main_category.id
    assert_not @link_url.valid?
  end

  test 'should allow both url and text' do
    @link_url.text = 'a' * 100
    assert @link_url.valid?
  end

  test 'should require either url or text' do
    @link_url.url = ''
    @link_url.text = ''
    assert_not @link_url.valid?
  end

  test 'category id should be present' do
    @link_url.category_id = ' '
    assert_not @link_url.valid?
  end

  test 'title should be present' do
    @link_url.title = ' '
    assert_not @link_url.valid?
  end

  test 'title should not be too short' do
    @link_url.title = 'a' * 2
    assert_not @link_url.valid?
  end

  test 'title should not be too long' do
    @link_url.title = 'a' * 256
    assert_not @link_url.valid?
  end

  test 'text should not be too short' do
    @link_text.text = 'a' * 4
    assert_not @link_text.valid?
  end

  test 'url should not be blank' do
    @link_url.url = ' ' * 20
    assert_not @link_url.valid?
  end

  test 'url should accept other protocols' do
    @link_url.url = 'ftp://www.site.com'
    assert @link_url.valid?
    @link_text.url = 'magnet:?xt=urn:sha1:0b834a7c3adb917a47d866197b54b99926b256dd&dn=OOo_2.1.0_Win32Intel_install_en-US.exe&xs=http://mirror.switch.ch/ftp/mirror/OpenOffice/stable/2.1.0/OOo_2.1.0_Win32Intel_install_en-US.exe'
    assert @link_text.valid?
  end
end
