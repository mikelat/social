Rails.env ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'bcrypt'
require 'json'
require 'minitest/reporters'
Minitest::Reporters.use!

module PasswordHelper
  def default_password_digest
    BCrypt::Password.create default_password, cost: 4
  end

  def default_password
    'password'
  end
end

class ActiveSupport::TestCase
  include PasswordHelper
  fixtures :all
  ActiveRecord::FixtureSet.context_class.send :include, PasswordHelper

  def body_json
    JSON.parse response.body
  end

  def submit_successful
    body_json['success'] == true
  end

  def submit_failure_sign_in_required
    body_json['success'] == false && body_json['sign_in_required'] == true
  end

  def logged_in?
    !session[:user_id].nil?
  end

  def log_in_as(user, options = {})
    password = options[:password] || default_password
    remember = options[:remember] || '0'
    if integration_test?
      post sessions_path, session: { name: user.name, password: password, remember: remember }
    else
      session[:user_id] = user.id
    end
  end

  private

  def integration_test?
    !!(defined? post_via_redirect)
  end
end
