require 'test_helper'

class LinksControllerTest < ActionController::TestCase
  def setup
    @sub_category = categories :sub
    @link = links :google
    @text = links :text
  end

  test 'should get index' do
    get :index
    assert_response :success
  end

  test 'new requires login' do
    get :new
    assert_redirected_to sign_in_path
  end

  test 'create requires login' do
    post :create
    assert submit_failure_sign_in_required
  end
end
