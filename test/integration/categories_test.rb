class CategoriesTest < ActionDispatch::IntegrationTest
  def setup
    @main_category = categories(:main)
    @sub_category = categories(:sub)
  end

  test 'should get main category' do
    get @main_category.path
    assert_response :success
  end

  test 'should get sub category' do
    get @sub_category.path
    assert_response :success
  end
end
