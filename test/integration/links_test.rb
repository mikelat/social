class LinksTest < ActionDispatch::IntegrationTest
  def setup
    @sub_category = categories :sub
    @link = links :google
    @text = links :text
  end

  test 'should get link comments page' do
    get @sub_category.path + '/' + @link.slug
    assert_response :success
  end

  test 'should get text comments page' do
    get @sub_category.path + '/' + @text.slug
    assert_response :success
  end

  test 'invalid submission' do
    log_in_as users(:mike)
    get submit_path
    assert_no_difference 'Link.unscoped.count' do
      post links_path, link: { title: '',
                               category_path: '/notapath',
                               text: '',
                               url: 'not a url' }
    end
    assert_not submit_successful
  end

  test 'valid link submission' do
    log_in_as users(:mike)
    get submit_path
    assert_difference 'Link.unscoped.count', 1 do
      post links_path, link: { title:  'a' * 5,
                               category_path: @sub_category.path,
                               text: '',
                               url: 'http://www.google.com' }
    end
    assert submit_successful
  end

  test 'valid text submission' do
    log_in_as users(:mike)
    get submit_path
    assert_difference 'Link.unscoped.count', 1 do
      post links_path, link: { title: 'a' * 5,
                               category_path: @sub_category.path,
                               text: 'a' * 15,
                               url: '' }
    end
    assert submit_successful
  end

  test 'should get comments' do
    get @link.comments_url
    assert_response :success
    get @text.comments_url
    assert_response :success
  end
end
