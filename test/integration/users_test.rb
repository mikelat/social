class SessionsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :mike
  end

  test 'login with invalid information' do
    get sign_in_path
    assert_template 'users/new'
    post sessions_path, session: { name: '', password: '' }
    assert_not submit_successful
  end

  test 'login with remember me' do
    log_in_as @user, remember: '1'
    assert_not_nil cookies['token']
  end

  test 'login without remember me' do
    log_in_as @user, remember: '0'
    assert_nil cookies['token']
  end

  test 'login then logout with remember me' do
    log_in_as @user, remember: '1'
    delete sign_out_path
    # Rails seems to set cookies to blank string with a expiry date in the past
    # so all we can do here is check if the cookies are blanked out
    assert_empty cookies['token']
    assert_empty cookies['user']
  end

  test 'invalid signup information' do
    get sign_in_path
    assert_no_difference 'User.count' do
      post users_path, user: { name: '',
                               email: 'not@an_email',
                               password: '123',
                               password_confirmation: '456' }
    end
    assert_not submit_successful
  end

  test 'valid signup information' do
    get sign_in_path
    assert_difference 'User.count', 1 do
      post users_path, user: { name: 'valid_user',
                               email: 'user@example.com',
                               password: default_password,
                               password_confirmation: default_password }
    end
    assert submit_successful
  end
end
