require "erb"
include ERB::Util
include ActionView::Helpers::TextHelper

module MarkdownParser
  REGEX = {
    simple: [
      [/(?<=\s|^)__(.+)__(?=\s|$)/, '<strong><!-- 1 --></strong>'],
      [/(?<=\s|^)\*\*(.+)\*\*(?=\s|$)/, '<strong><!-- 1 --></strong>'],
      [/(?<=\s|^|\>|\<)_(.+)_(?=\s|$|\>|\<)/, '<em><!-- 1 --></em>'],
      [/(?<=\s|^|\>|\<)\*(.+)\*(?=\s|$|\>|\<)/, '<em><!-- 1 --></em>'],
      [/(?<=\s|^|\>|\<)~~(.+)~~(?=\s|$|\>|\<)/, '<del><!-- 1 --></del>'],
      [/\^(.+)\^/, '<sup><!-- 1 --></sup>'],
      [/~(.+)~/, '<sub><!-- 1 --></sub>']
    ],
    url: /\[(.+?)\]\(([^<>\'\"\s]+?)\)/,
    autolink: /(?<=\s|^)((http:\/\/|https:\/\/|ftp:\/\/|www\.).+?)(?=\s|$)/,
    local_autolink: /(?<=\s|^)(\/[a-zA-Z0-9\/\-]+)(?=\s|$)/,
    blockquote: [/(?<=^|\<blockquote\>)+\s*(&gt;.+(\n|$))+/, '&gt;', '<blockquote><!-- 1 --></blockquote>'],
    list: /((?<=^|\r\n|\n)[^\S\n]*(\d\.|\*|\-)(.+?(\r\n|\n|$)))+/
  }

  # Prevents infinite embedding of elements that potentially can ruin the browser
  RECURSIVE_LIMIT = 3

  def self.markdown_convert(text)
    # Escaping this string converts it to SafeBuffer, which messes with
    # the regex below. We have to force it to be a string.
    text = html_escape(text).to_str

    REGEX[:simple].each do |markdown|
      text.gsub!(markdown[0], markdown[1].gsub('<!-- 1 -->', '\\\\1'))
    end

    parse_list! text
    parse_blockquote! text

    text.gsub!(REGEX[:url]) { parse_link($2, $1) }
    text.gsub!(REGEX[:autolink]) { parse_link($1) }
    text.gsub!(REGEX[:local_autolink]) { parse_link($1) }

    text = simple_format(text, {}, sanitize: false)
    #text.gsub! /\n/, '<br />'
    text.html_safe
  end

  private

  def self.parse_link(url, title=nil)
    title ||= url
    url = html_escape(url).gsub('&amp;', '&')
    '<a href="' + url + '" target="_blank">' + title + '</a>'
  end

  def self.parse_list!(text, counter=0)
    return text if counter == RECURSIVE_LIMIT
    text.gsub!(REGEX[:list]) do |matched|
      list_data = /^(\s)*(\d|\*|\-)\.?.+?/.match(matched)
      unless list_data.nil?
        # Clean up string of trailing whitespace and the first list delimiter
        matched.strip!
        matched.gsub! /\A\s*(\d\.|\*|\-)\s/, ''

        matched = matched.split(/(?:\r\n|\n)#{list_data[1]}(?:\d\.|\*|\-)\s*/)
        matched.map { |x| parse_list! x, counter + 1 }
        matched = matched.join('</li><li>')

        list_type = ['*', '-'].include?(list_data[2]) ? 'ul' : 'ol'
        matched = "<#{list_type}><li>" + matched + "</li></#{list_type}>"
      end
      matched
    end
  end

  def self.parse_blockquote!(text, counter=0)
    return text if counter == RECURSIVE_LIMIT
    text.gsub!(REGEX[:blockquote][0]) do |matched|
      REGEX[:blockquote][2].gsub '<!-- 1 -->', matched.gsub(/^\s*#{REGEX[:blockquote][1]}/, '')
    end
    parse_blockquote! text, counter + 1
  end
end
