require 'securerandom'
require 'lorem_ipsum_amet'

@categories = nil
@user_ids = nil

def rng_time
  Time.at(rand((3.days.ago.to_i)..(Time.now.to_i)))
end

def rng_comment(num, depth=0)
    comments = Comment.where(depth: depth)

    num.times do |i|
      comment = comments.sample
      Comment.create!(
        text: LoremIpsum.w(rand(5..100)),
        comment_id: comment.id,
        link_id: comment.link_id,
        user_id: @user_ids.sample,
        score_up: rand(0..60),
        score_down: rand(0..30),
        ip_address: '0.0.0.0',
        created_at: rng_time)
    end
end

def create_link!(*args)
  Link.create!(args[0].merge({
    user_id: rand(1..8),
    category_id: @categories.sample.id,
    score_up: rand(0..120),
    score_down: rand(0..60),
    nsfw: rand(1..4) == 1,
    created_at: rng_time }))
end

task data: :environment do
  Rails.logger.level = Logger::DEBUG
  `rm -rf public/thumbnails/*`
  games = Category.create!(name: 'Games')
  Category.create!(name: 'PC', category_id: games.id)
  Category.create!(name: 'Xbox', category_id: games.id)
  Category.create!(name: 'Playstation', category_id: games.id)
  Category.create!(name: 'Nintendo', category_id: games.id)
  Category.create!(name: 'Dota 2', category_id: games.id)
  Category.create!(name: 'Cat with Spaces', category_id: games.id)

  tech = Category.create!(name: 'Technology')
  Category.create!(name: 'Gadgets', category_id: tech.id)
  Category.create!(name: 'Software', category_id: tech.id)
  Category.create!(name: 'Hardware', category_id: tech.id)
  Category.create!(name: 'Programming', category_id: tech.id)

  sci = Category.create!(name: 'Science')
  Category.create!(name: 'Astronomy', category_id: sci.id)
  Category.create!(name: 'Biology', category_id: sci.id)
  Category.create!(name: 'Physics', category_id: sci.id)

  fun = Category.create!(name: 'Funny')
  Category.create!(name: 'Pictures', category_id: fun.id)
  Category.create!(name: 'Videos', category_id: fun.id)
  Category.create!(name: 'Jokes', category_id: fun.id)

  biz = Category.create!(name: 'Business')
  Category.create!(name: 'Entrepreneurs', category_id: biz.id)
  Category.create!(name: 'Finance', category_id: biz.id)

  @categories = Category.where('category_id != 0')

  Link.uncached do
    create_link!(title: 'Netflix launched a new layout!', url: 'https://movies.netflix.com/')
    create_link!(title: 'Some tech article and a really long name in the title to test the trim of slugs thx', url: 'http://techcrunch.com/2015/08/18/when-the-bus-will-be-its-own-boss/')
    create_link!(title: 'I put code here sometimes', url: 'https://www.gitlab.com/')
    create_link!(title: 'Oh hey look its a link to google', url: 'http://www.google.com')
    create_link!(title: 'Path of exile got a 2.0 release!', url: 'http://www.pathofexile.com')
    create_link!(title: 'A picture of a crazy guy', url: 'http://i.imgur.com/NH0kxQ6.jpg')
    create_link!(title: 'mario party 2', url: 'https://www.youtube.com/watch?v=m6PxRwgjzZw&')
    create_link!(title: 'hello', url: 'https://www.youtube.com/watch?v=NL6CDFn2i3I')
    create_link!(title: 'this is a random youtube link', url: 'https://www.youtube.com/watch?v=E8sxwK2pJI4')
    create_link!(title: 'this is a random youtube link', url: 'https://www.youtube.com/watch?v=E8sxwK2pJI4')
    create_link!(title: 'a song to test embed', url: 'https://soundcloud.com/neal-acree/starcraft-ii-heart-of-the')
    create_link!(title: 'textual post', text: 'this is a text post')
    create_link!(title: '!!!!!!!!', text: 'this is a text post')
    create_link!(title: 'link to google and text', url: 'http://www.google.com', text: 'text included!')

    User.create!(name: 'lat', admin: true, weight_up: 10, weight_down: 20, password: 'password', ip_address: '0.0.0.0')
    User.create!(name: 'pat', admin: true, weight_up: 5, weight_down: 10, password: 'password', ip_address: '0.0.0.0')
    8.times do |i|
      User.create!(name: 'user-' + (i + 1).to_s, weight_up: 2, weight_down: 0, password: 'password', ip_address: '0.0.0.0')
    end

    link_ids = Link.pluck(:id)
    @user_ids = User.pluck(:id)

    75.times do |i|
      Comment.create!(
        text: LoremIpsum.w(rand(5..100)),
        link_id: link_ids.sample,
        user_id: @user_ids.sample,
        score_up: rand(0..60),
        score_down: rand(0..30),
        ip_address: '0.0.0.0',
        created_at: rng_time)
    end

    rng_comment(75, 0)
    rng_comment(50, 1)
    rng_comment(50, 2)
    rng_comment(40, 3)
  end
end
