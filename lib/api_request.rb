module API_Request
  def self.get(uri_str, limit = 5)
    raise 'Too many HTTP redirects' if limit == 0

    response = Net::HTTP.get_response(URI(uri_str))

    case response
    when Net::HTTPSuccess then
      response
    when Net::HTTPRedirection then
      location = response['location']
      # warn "redirected to #{location}"
      get(location, limit - 1)
    else
      response.value
    end
  end
end
