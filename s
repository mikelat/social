if [ "$RAILS_ENV" != "production" ]; then
	killall ruby
	bin/delayed_job start
	rails s -b 0.0.0.0
else
	echo "You're still on prod!!!"
fi
