class Vote < ActiveRecord::Base
  belongs_to :comment
  belongs_to :link
  belongs_to :user

  validates :user, :presence => true
  validate :only_one_content_type
  validate :not_owned_by_user

  before_create :set_weight
  after_save :update_content_model

  MODELS_FROM_ID = {
    link_id: 'Link',
    comment_id: 'Comment'
  }

  def content_id
    self.attributes[content_type]
  end

  def content_type
    self.attributes.each do |name, value|
      if name.match(/[a-z]+_id/) and !value.nil? and name != 'user_id'
        return name
      end
    end
  end

  def referer=(value)
    write_attribute :referer, value.slice(0, 255) unless value.blank?
  end

  private

  def update_content_model
    if change_changed?
      score_diff = 0
      updates = []
      # Apply this score change
      if change == 1
        updates << 'score_up=score_up+' + weight_up.to_s
      elsif change == -1
        updates << 'score_down=score_down+' + weight_down.to_s
      end

      # Reverse last score change
      if change_was == 1
        updates << 'score_up=score_up-' + weight_up.to_s
      elsif change_was == -1
        updates << 'score_down=score_down-' + weight_down.to_s
      end

      unless updates.empty?
        MODELS_FROM_ID[content_type.to_sym].constantize.where(id: content_id)
          .update_all(updates.join(','))
      end
    end
  end

  def only_one_content_type
    attributes_changed = 0
    self.attributes.each do |name, value|
      if name.match(/[a-z]+_id/) and !value.nil? and name != 'user_id'
        attributes_changed += 1
      end
    end
    errors.add :base, :invalid if attributes_changed > 1
  end

  def not_owned_by_user
    content = MODELS_FROM_ID[content_type.to_sym].constantize.find_by(id: content_id)
    errors.add :base, :invalid if content.user_id == user_id
  end

  def set_weight
    user = User.find user_id
    self.weight_up = user.weight_up
    self.weight_down = user.weight_down
  end
end
