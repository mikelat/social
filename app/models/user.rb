class User < ActiveRecord::Base
  validates :name, presence: true,
                   length: { in: 3..25 },
                   format: { with: /[a-z0-9\-]+/i },
                   uniqueness: { case_sensitive: false }
  validates :email, allow_blank: true,
                    length: { maximum: 255 },
                    format: { with:  /\A[\w+\-.]+@[a-z\-.]+\.[a-z]+\z/i }
  validates :password, length: { minimum: 6 }, if: :password_digest_changed?
  validates :ip_address, presence: true

  before_save :name_to_lower

  cattr_accessor :current

  has_many :user_auths
  has_secure_password

  def valid_token?(token)
    UserAuth.find_by user_id: id, auth_key: token
  end

  def create_token
    token = SecureRandom.urlsafe_base64(50)
    UserAuth.create user_id: id, auth_key: token
    token
  end

  def to_param
    name_lower
  end

  def profile_url
    '/users/' + name_lower
  end

  def can_edit?(user)
    !user.nil? && !user.id.nil? && (user.admin || user.id == user_id)
  end

  private

  def name_to_lower
    self.name_lower = name.downcase
  end
end
