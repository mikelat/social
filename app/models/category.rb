class Category < ActiveRecord::Base
  validates :name, presence: true

  has_many :children, class_name: 'Category',
                      foreign_key: 'category_id'

  belongs_to :parent, class_name: 'Category',
                      foreign_key: 'category_id'

  before_create :set_category_name

  RESERVED_NAMES = %w(submit top new unread)

  # TODO: Change to fetch default categories instead of all
  def self.default_by_parent
    # TODO: Categories should have better cache
    Rails.cache.fetch('categories', expires_in: 1.minutes) do
      category_by_parent = []
      Category.order('category_id, name').each do |category|
        category_by_parent[category.category_id.to_i] ||= []
        category_by_parent[category.category_id.to_i] << category
      end
      category_by_parent
    end
  end

  def self.generate_path(category, subcategory)
    return if category.nil? || RESERVED_NAMES.include?(category)
    unless subcategory.nil? || RESERVED_NAMES.include?(subcategory)
      category << '/' + subcategory unless subcategory.nil?
    end
    '/' + category.downcase
  end

  # TODO: replace with actual category images later
  def icon
    case path
    when '/business'
      'briefcase'
    when '/funny'
      'bomb'
    when '/games'
      'gamepad'
    when '/science'
      'graduation-cap'
    when '/technology'
      'desktop'
    else
      'question'
    end
  end

  private

  # Creates full path to category
  def set_category_name
    name_slugged = name.downcase.strip
        .gsub(/[^A-Za-z0-9]+/, '-')
        .gsub(/-([0-9])+/, '\\1')
    if parent.nil?
      self.path = '/' + name_slugged
    else
      self.path = parent.path + '/' + name_slugged
    end
    self.path_indexed = path.gsub '-', ''
  end
end
