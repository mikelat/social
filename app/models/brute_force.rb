class BruteForce < ActiveRecord::Base
  # We only care about brute force attempts default timespan
  #default_scope {  }

  scope :within_timeout, -> do
    where 'created_at > ?', Time.now - TIMEOUT
  end

  LOCKED_BAN_THRESHOLD = 15 # Score required to get temporarily banned
  LOCKED_RISK_THRESHOLD = 5 # Score required to start getting captchas
  LOCKED_INCREMENT = 1

  HIDDEN_THESHOLD = 3 # Separate counter for hidden services
  HIDDEN_INCREMENT = 1

  TIMEOUT = 15.minutes

  def banned?
    score > LOCKED_BAN_THRESHOLD
  end

  def risky?
    score > LOCKED_RISK_THRESHOLD
  end

  def hidden_banned?
    hidden_score > HIDDEN_THESHOLD
  end

  def time_left_in_words
    time_ago_in_words created_at.to_time + TIMEOUT
  end

  def increment!(amount = LOCKED_INCREMENT)
    self.score += amount
    self.save!
    self.score
  end

  def hidden_increment!(amount = HIDDEN_INCREMENT)
    self.hidden_score += amount
    self.save!
    self.hidden_score
  end
end
