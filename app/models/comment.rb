class Comment < ActiveRecord::Base
  validates :text, presence: true, length: { minimum: 1 }

  before_save :cache_markdown

  belongs_to :link, counter_cache: true
  belongs_to :user

  has_many :children, class_name: 'Comment',
                      foreign_key: 'comment_id'

  belongs_to :parent, class_name: 'Comment',
                      foreign_key: 'comment_id'

  before_create :set_depth
  before_create :set_link_id

  scope :for_link, -> (link_id, user_id) do
    order('comments.score, comments.created_at')
    .joins('LEFT JOIN votes on votes.comment_id=comments.id AND votes.user_id=' + user_id.to_i.to_s)
    .select(['votes.change', 'comments.*'].join(', '))
    .where(link_id: link_id)
  end

  default_scope do
    joins(:user).
      select(['users.name as user_name', 'comments.*'].join(', '))
  end

  EDIT_TIME_LIMIT = 2.hours
  DELETE_TIME_LIMIT = 2.hours

  def self.by_parent(link_id, user_id)
    by_parent = {}
    Comment.for_link(link_id, user_id).find_each do |item|
      parent_id = item[:comment_id] ? item[:comment_id] : 0
      by_parent[parent_id] ||= []
      by_parent[parent_id] << item
    end
    by_parent
  end

  def can_edit?(user)
    !user.nil? && (user.admin || (created_at < EDIT_TIME_LIMIT.ago && user.id == user_id))
  end

  def can_delete?(user)
    !user.nil? && (user.admin || (created_at < DELETE_TIME_LIMIT.ago && user.id == user_id))
  end

  def score
    score_up - score_down
  end

  private

  def set_link_id
    self.link_id = parent.link_id unless parent.nil?
  end

  def set_depth
    self.depth = parent.nil? ? 0 : parent.depth + 1
  end

  def cache_markdown
    self.text_cached = MarkdownParser.markdown_convert text
  end
end
