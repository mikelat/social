require 'shellwords'
require 'open-uri'
require 'json'

class Link < ActiveRecord::Base
  validate :valid_category
  validates :title, presence: true, length: 3..255
  validates :text, allow_blank: true, length: { minimum: 5 }
  validates :category, presence: true
  validate :either_url_or_text

  before_save :set_content_type
  after_create :generate_thumbnail, unless: Proc.new { text? || url =~ BANNED_THUMBNAILS }
  after_create :set_slug

  belongs_to :user
  belongs_to :category

  has_many :votes
  has_many :comments

  scope :full, -> (user_id) do
    joins(:user, :category).
      joins('LEFT JOIN votes on votes.link_id = links.id AND votes.user_id=' + user_id.to_i.to_s).
      select(['users.name as user_name', 'categories.name as category_name',
      'categories.path as category_path', 'votes.change', 'links.*'].join(', '))
  end

  CONTENT_TYPES = [
    { type: :url, icon: nil, class: '' },
    { type: :text, icon: nil, class: 'text' },
    { type: :image, icon: 'camera', class: 'image' },
    { type: :video, icon: 'video-camera', class: 'video' },
    { type: :audio, icon: 'volume-up', class: 'audio' }
  ]

  MAX_SLUG_LENGTH = 50
  BANNED_THUMBNAILS = /(localhost|127\.0\.[0-1]\.[0-1]|192\.168\.[0-9\.].[0-9\.]+)$/i

  # TODO: Will refactor this later to be DB driven
  MEDIA = {
    'youtube.com': {
      content_type: 3,
      regex: /youtube\.com\/watch\?v=([A-Za-z0-9\-_]+)/i,
      embed: '<iframe class="iframe-embed" width="560" height="315" src="https://www.youtube.com/embed/<!-- ID -->?autoplay=1" allowfullscreen></iframe>',
      thumbnail: 'https://img.youtube.com/vi/<!-- ID -->/mqdefault.jpg'
    },
    'youtu.be': {
      content_type: 3,
      regex: /youtu\.be\/([A-Za-z0-9\-_]+)/i,
      embed: '<iframe class="iframe-embed" width="560" height="315" src="https://www.youtube.com/embed/<!-- ID -->?autoplay=1" allowfullscreen></iframe>',
      thumbnail: 'https://img.youtube.com/vi/<!-- ID -->/mqdefault.jpg'
    },
    'soundcloud.com': {
      content_type: 4,
      regex: /(soundcloud.com\/.+)/i,
      embed: '<iframe width="100%" height="166" scrolling="no" frameborder="0" src="https://w.soundcloud.com/player/?url=<!-- ID -->&amp;color=ff5500&amp;auto_play=true&amp;hide_related=false&amp;show_comments=false&amp;show_user=false&amp;show_reposts=false"></iframe>'
    }
  }
  IMAGE_MEDIA = {
    valid_domains: ['i.imgur.com'],
    valid_exts: /\.(jpg|jpeg|gif|png)$/
  }

  def content_by_type(type)
    CONTENT_TYPES.each_with_index do |content, key|
      return key if content[:type] == type
    end
  end

  def url?
    CONTENT_TYPES[content_type][:type] == :url
  end

  def text?
    CONTENT_TYPES[content_type][:type] == :text
  end

  def image?
    CONTENT_TYPES[content_type][:type] == :image
  end

  def video?
    CONTENT_TYPES[content_type][:type] == :video
  end

  def audio?
    CONTENT_TYPES[content_type][:type] == :audio
  end

  def score
    score_up - score_down
  end

  def url=(value)
    value.strip!
    # Assume http if no other scheme detected
    value = 'http://' + value unless value.blank? || value =~ /^[a-zA-Z]+:.+/
    write_attribute :url, value
  end

  def category_path=(value)
    value.insert(0, '/') unless value.starts_with?('/')
    category = Category.find_by path_indexed: value.gsub('-', '').downcase
    self.category_id = category.id unless category.nil?
  end

  def url_or_comments
    text? ? comments_url : read_attribute(:url)
  end

  def comments_url
    (read_attribute(:category_path).nil? ? category.path : category_path) + '/' + slug
  end

  def self.get_links(user = nil, category = nil)
    q = full(user).order(created_at: :desc)
    unless category.nil?
      category_ids = [category.id]
      unless category.category_id
        category_ids += Category.where(category_id: category.id).pluck(:id)
      end
      q = q.where category_id: category_ids
    end
    q.all
  end

  def content_icon
    CONTENT_TYPES[content_type][:icon] unless content_type.nil?
  end

  def content_class
    (' ' + CONTENT_TYPES[content_type][:class]) if content_type > 0
  end

  def slug
    return read_attribute(:slug) unless read_attribute(:slug).nil?
    slug = title.squish.gsub(/[^A-Za-z0-9]+/, '-').chomp('-')
                .reverse.chomp('-').reverse.downcase[0..MAX_SLUG_LENGTH - 1]
    slug = Digest::SHA1.hexdigest(url.to_s + text.to_s)[0..10] if slug.blank?

    if slug.rindex('-').to_i > 30 && slug.length >= MAX_SLUG_LENGTH
      slug = slug[0..slug.rindex('-') - 1]
    end

    slug += '-' + id.to_s(36)
  end

  def self.find_by_slug!(slug)
    id = slug.split('-')[-1].to_i(36)
    link = Link.find id
    raise ActiveRecord::RecordNotFound if link.slug != slug
    link
  end

  def comments_by_parent(user_id)
    @comments_by_parent_cached ||= Comment.by_parent id, user_id
  end

  def thumbnail
    return '/text.png' if text?
    return '/noimg.png' if !has_thumbnail && !generating_thumbnail?
    "/thumbnails/#{id}.png"
  end

  def generating_thumbnail?
    # We give the thumbnail generator 20 seconds before timing this out and just showing a regular
    # "no image for this link" image
    created_at > 20.seconds.ago && !text? && !has_thumbnail
  end

  def generate_thumbnail
    url_safe = Shellwords.escape url
    file = "/tmp/#{id}.png"
    # Embedded media with a thumbnail
    if !MEDIA[domain.to_sym].nil? && !MEDIA[domain.to_sym][:thumbnail].nil? && url =~ MEDIA[domain.to_sym][:regex]
      url_safe = MEDIA[domain.to_sym][:thumbnail].gsub('<!-- ID -->', url.match(MEDIA[domain.to_sym][:regex])[1])
    # Soundcloud API for thumbnail
    elsif domain == 'soundcloud.com'
      api_response = JSON.parse(API_Request::get("http://api.soundcloud.com/resolve?url=#{url}&client_id=" +
        Rails.application.secrets.soundcloud_api_key).body)
      url_safe = api_response['artwork_url'] unless api_response['artwork_url'].blank?
    end

    # Just download the image instead of entire page
    if url_safe =~ IMAGE_MEDIA[:valid_exts]
      File.open(file, 'wb') do |fo|
        fo.write open(url_safe).read
      end
    # Download page and generate thumbnail
    else
      if !system "timeout 20 wkhtmltoimage -n --height 720 --width 1280 #{url_safe} #{file}"
        fail "Could not generate thumbnail for #{url_safe}! Is wkhtmltoimage installed?"
      end
    end

    image = MiniMagick::Image.open file
    image.resize '80x45'
    image.write Rails.root.join 'public/', 'thumbnails/' "#{id}.png"
    self.has_thumbnail = true
    self.save!
  # Normally exceptions are emailed, but not when its a async background task so we add it here.
  rescue => exception
    ExceptionNotifier.notify_exception(exception)
    self.thumbnail_failed = true
    self.save!
  end
  handle_asynchronously :generate_thumbnail

  private

  def valid_category
    if category.nil? || category.id.nil?
      errors.add :category, :not_found
    elsif category.category_id.nil?
      errors.add :base, :main_category
    end
  end

  def either_url_or_text
    if read_attribute(:url).blank? && text.blank?
      errors.add :base, :need_url_or_text
    #elsif !read_attribute(:url).blank? && !text.blank?
      #errors.add :base, :no_url_and_text
    end
  end

  def set_content_type
    if read_attribute(:url).blank?
      self.content_type = content_by_type :text
      self.domain = 'text' + category.path.gsub('/', '.')
      return
    end
    self.domain = URI.parse(url).host.sub(/^(www.)?(.*)/, '\2').downcase
    if IMAGE_MEDIA[:valid_domains].include?(domain)
      self.content_type = content_by_type :image
    elsif !MEDIA[domain.to_sym].nil?
      unless url !~ MEDIA[domain.to_sym][:regex]
        self.content_type = MEDIA[domain.to_sym][:content_type]
      end
    end
  end

  def set_slug
    write_attribute :slug, slug
    self.save!
  end
end
