loaded_time = 0

$(document).on 'app:ready', (obj, context) ->
  return unless $('body.c-links').length
  loaded_time = Date.now()
  $('.link.generating-thumbnail').each ->
    try_thumbnail $(this), loaded_time

try_thumbnail = (link, loaded_time) ->
  $.get($('.thumbnail img', link).attr('src'))
    .success ->
      link.removeClass('generating-thumbnail')
      $('.thumbnail img', link)
        .attr('src', $('.thumbnail img', link).attr('src') + '?' + Date.now())
    .error ->
      # Will only try to load thumbnail within 20 seconds
      if Date.now() - 20000 > loaded_time
        $('.thumbnail img', link).attr('src', '/noimg.png')
        link.removeClass('generating-thumbnail')
      else
        setTimeout ( -> try_thumbnail(link)), 2000
