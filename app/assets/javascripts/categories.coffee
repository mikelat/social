$(document)
  .on 'app:ready', (obj, context) ->
    ###
    $('.view', context).click ->
      link = $(this).parents('.link')

      if !$('.view-content', link).length
        host = $(this)[0].host
        host = host[4..-1] if host[0..3] == 'www.'
        # Regex replace media content
        if $(window).data('media')['regex'][host]?
          regex = $(window).data('media')['regex'][host]
          id = $(this).attr('href').match(new RegExp(regex[1], 'i'))
          content = regex[2].replace('<!-- ID -->', encodeURIComponent(id[1]))
        # Basic image from whitelisted domains
        if $(this).attr('href').match(new RegExp($(window).data('media')['valid_image_exts'], 'i')) != null and host in $(window).data('media')['valid_image_domains']
          content = "<img src=\"#{$(this).attr('href')}\"></img>"
        # Text post
        if link.hasClass('text')
          content = "No handling has been built for getting text posts but I'm building it I promise."

        #if link.hasClass('text') or link.hasClass('audio')
        link.append "<div class=\"view-content fit-content\">#{content}</div>"

        else if link.hasClass('image') or link.hasClass('video')
          title = $('.title', link).html()
          $('body').addClass('modal').append "
            <div id=\"modal_background\"></div>
            <div id=\"modal\" class=\"fit-content\" onclick=\"return close_modal()\">
              <div id=\"modal_title\">
                <a id=\"modal_close\" href=\"#\" onclick=\"return close_modal()\">
                  <i class=\"fa fa-times\"></i>
                </a>
                #{title}
              </div>
              #{content}
              <div class=\"cb\"></div>
            </div>"

        $(document).trigger 'app:resize'
      else
        $('.view-content', link).remove()
      false
    ###

get_param = (url, name=null) ->
  queries = {}
  $.each(url.substr(1).split('&'), (c,q) ->
    i = q.split('=')
    queries[i[0].toString()] = i[1].toString()
  );
  if name == null
    queries
  else
    queries[name] if queries[name]?
