$(document)
  .on 'app:ready', (obj, context) ->
    return unless $('body.c-links.a-show').length
    $('.reply a, .edit a', '.comment').click ->
      type = if $(this).parent().hasClass('edit') then 'edit' else 'reply'
      id = $(this).parents('.comment').prop('id').match(/comment_([0-9]+)/)[1]
      if $("##{type}_form_" + id).length
        $("##{type}_form_" + id).toggle()
      else
        form = $("#comment_form").clone(true, true)
        form.prop('id', "#{type}_form_" + id).addClass("form-#{type}")
        $('[id]', form).each ->
          $(this).prop('id', $(this).prop('id') + '_' + id)
        $('input[name="comment[comment_id]"]', form).val(id)
        $('.errors', form).remove()
        if type == 'edit'
          form.attr('action', $(this).attr('href'))
          form.prepend('<input type="hidden" name="_method" value="patch">')
        $('#comment_' + id + ' > .markdown').after(form)
      if $("##{type}_form_" + id).is(':visible')
        $('textarea', "##{type}_form_" + id).focus()
        if type == 'edit'
          $('textarea', "##{type}_form_" + id).val($(this).data('content'))
        #window.location = '#comment_form_' + id
      false
    $('#comment_form').on 'ajax:success', (obj, data) ->
      if $(this).hasClass('form-edit')
        comment = $(this).parents('.comment')
        $('.comment-markdown', comment).html(data.text_cached)
        $('.edit a', comment).data('content', data.text)
        $('.form-edit', comment).hide()
