$(document).on 'app:ready', (obj, context) ->
  return unless $('body.c-users').length
  $('#settings_form input').change ->
    $('#settings_form').submit()
    if $(this).is(':checked')
      $('body').removeClass($(this).data('body-off'))
      $('body').addClass($(this).data('body-on'))
    else
      $('body').removeClass($(this).data('body-on'))
      $('body').addClass($(this).data('body-off'))
