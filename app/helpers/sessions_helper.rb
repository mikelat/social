module SessionsHelper
  attr_writer :current_user
  @current_user = nil

  def sign_in(user, remember=false)
    if remember
      cookies.permanent[:token] = user.create_token
      cookies.permanent[:user] = user.name_lower
    else
      cookies.delete :token
      cookies.delete :user
    end
    session[:user_id] = user.id
    @current_user = user
  end

  def signed_in?
    !current_user.nil?
  end

  def current_user
    @current_user ||= User.find_by_id session[:user_id] if session[:user_id]
  end

  def current_user?(user)
    user == current_user
  end

  def current_user_id
    signed_in? ? current_user.id : 0
  end

  def sign_out
    return unless current_user.id
    if cookies[:token]
      ua = UserAuth.find_by(user_id: current_user.id, auth_key: cookies[:token])
      ua.destroy unless ua.nil?
    end
    cookies.delete :token
    cookies.delete :user
    session[:user_id] = nil
    @current_user = nil
  end
end
