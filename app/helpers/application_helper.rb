module ApplicationHelper
  GOOGLE_API = 'https://www.google.com/recaptcha/api/siteverify?'

  # Verifies against recaptcha API
  def verify_captcha(model=nil, param_name='g-recaptcha-response')
    # Don't check captcha in dev or testing
    if Rails.env.development? || Rails.env.test?
      return true
    end

    url = URI.parse(GOOGLE_API + 'secret=' +
      Rails.application.config.private_key +
      '&response=' + params[param_name] +
      '&remoteip=' + request.remote_ip)
    result = JSON.parse Net::HTTP.get(url)

    return true if result['success'] == true
    error = ''

    if result['error-codes'].include? 'missing-input-response'
      error = t('errors.captcha.empty')
    elsif result['error-codes'].include? 'invalid-input-response'
      error = t('errors.captcha.invalid')
    else
      fail 'Unknown captcha error'
    end

    if model.nil?
      return result['success'], t('errors.captcha.captcha') + ' ' + error
    else
      model.errors.add :captcha, error
      return result['success']
    end
  end

  # Parses json result for forms
  def json_result(model, **additional_data)
    model_errors = {}
    model.errors.messages.each do |name, error|
      model_errors[name] = model.errors.full_message name, error.first
    end
    { errors: model_errors, success: model_errors.empty? }.merge(additional_data)
  end

  def no_permission_redirect!
    flash[:error] = t('errors.no_permission')
    redirect_to(root_url)
  end
end
