module FormHelper
  # Conditional captcha if user is deemed "risky"
  def captcha(name, show_when_risky=false)
    data = {}
    if show_when_risky
      bf = BruteForce.within_timeout.find_or_create_by ip_address: request.remote_ip
      data[:score] = bf.score
    end
    content_tag :div, '', class: 'captcha', data: data, id: name + '_captcha'
  end

  def attribute_max(model, attribute)
    model.validators_on(attribute).each do |validator|
      return validator.options[:maximum] if validator.options.key? :maximum
    end
  end
end
