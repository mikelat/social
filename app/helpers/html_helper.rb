module HtmlHelper
  def alerts
    return unless flash.any?
    alerts = ''
    flash.each do |name, msg|
      if ['error', 'warning', 'info', 'success'].include?(name)
        alerts += content_tag :div, class: "alert alert-#{name}" do
          link_to(fa_tag('times'), '#', class: 'alert-hide') + content_tag(:p, msg)
        end
      end
    end
    return alerts
  end

  def body_classes
    classes = "c-#{controller.controller_name} a-#{controller.action_name}"
    classes += signed_in? ? ' signed-in' : ' signed-out'
    if signed_in?
      classes += ' can-downvote' if current_user.weight_down > 0
      classes += current_user.show_nsfw ? '' : ' hide-nsfw'
    end

    return classes
  end

  def fa_tag(tag)
    content_tag :i, '', class: 'fa fa-' + tag
  end

  def title_tag(title)
    if title.blank? || current_page?(root_url)
      return t(:site_name)
    else
      return title + ' :: ' + t(:site_name)
    end
  end

  def mini_title_tag(title, url, fallback)
    title ||= fallback
    return if title.nil?
    link_to(title.downcase, url, class: 'mini-title')
  end

  def markdown_example(text)
    content_tag(:dt, text) + content_tag(:dd, MarkdownParser.markdown_convert(text))
  end

  def votable_class(change)
    classes = ' votable'
    return classes if change.nil? || change == 0
    if change < 0
      return classes + ' downed'
    elsif change > 0
      return classes + ' upped'
    end
  end

  def signed_class(score)
    if score > 0
      return ' positive'
    elsif score < 0
      return ' negative'
    end
  end

  def mine_class(user_id)
    user_id == current_user_id ? ' mine' : ''
  end
end
