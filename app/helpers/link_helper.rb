module LinkHelper
  def link_info(link, show_full: false)
    language_args = {
      category: link_to(link.category_path, link.category_path),
      user: link_to(link.user_name, user_path(link.user_name.downcase)),
      created: time_ago_in_words(link.created_at)
    }
    # full info (on specific link comments page)
    if show_full
      t('.info_full_html', language_args)
    # no category info (on specific category page)
    elsif @category_path == link.category_path
      t('.info_basic_html', language_args.except(:category))
    # no user info (on homepage or the like)
    else
      t('.info_html', language_args.except(:user))
    end
  end

  def link_classes(link)
    classes = ''
    classes += mine_class(link.user_id)
    classes += votable_class(link.change)
    classes += link.content_class.to_s
    classes += link.generating_thumbnail? ? ' generating-thumbnail' : ''
    classes += link.nsfw ? ' nsfw' : ''
  end

  def comment_classes(comment)
    classes = ''
    classes += mine_class(comment.user_id)
    classes += votable_class(comment.change)
    classes += comment.deleted ? ' deleted' : ''
  end
end
