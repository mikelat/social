class VotesController < ApplicationController
  before_action :must_be_signed_in

  def create
    vote = Vote.find_or_initialize_by vote_params.merge({'user_id': current_user.id})
    vote.change = params['change']
    if vote.new_record?
      vote.last_action_at = session[:last_action_at]
      vote.ip_address = request.remote_ip
      vote.referer = session[:referer_track]
    end
    vote.save
    render json: json_result(vote)
  end

  private

  def vote_params
    params.permit(:link_id, :comment_id)
  end
end
