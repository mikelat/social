class CommentsController < ApplicationController
  before_action :must_be_signed_in
  before_action :permission_to_edit, only: :update
  before_action :permission_to_delete, only: :destroy

  def create
    comment = Comment.new params.require(:comment).permit(:text, :link_id, :comment_id)
    comment.ip_address = request.remote_ip
    comment.user_id = current_user.id
    redirect = comment.link.comments_url if comment.save # verify_captcha(link) &&
  end

  def update
    @comment.update params.require(:comment).permit(:text)
    render json: json_result(@comment, text_cached: @comment.text_cached, text: @comment.text)
  end

  def destroy
    @comment.update deleted: true
    # TODO: Should reload partial instead of redirect
    redirect_to @comment.link.comments_url
  end

  private

  def permission_to_edit
    @comment = Comment.find_by! id: params[:id]
    no_permission_redirect! unless @comment.can_edit?(current_user)
  end

  def permission_to_delete
    @comment = Comment.find_by! id: params[:id]
    no_permission_redirect! unless @comment.can_delete?(current_user)
  end
end
