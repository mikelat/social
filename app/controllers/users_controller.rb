class UsersController < ApplicationController
  before_action :permission_to_edit, only: [:edit, :update]
  before_action :permission_to_destroy, only: [:destroy]

  def new
  end

  def create
    user = User.new params.require(:user).permit(:name, :email, :password, :password_confirmation)
    user.ip_address = request.remote_ip
    if verify_captcha(user) && user.save
      sign_in user, remember: params[:user][:remember]
      flash[:success] = t '.success_message', user: user.name
    end
    render json: json_result(user, redirect: root_path)
  end

  def show
    @user = User.find_by! name_lower: params[:id]
    @title = @user.name
  end

  def edit
  end

  def update
    if !params[:user][:update_password].nil? # Updating password
      bf = BruteForce.within_timeout.first_or_initialize ip_address: request.remote_ip

      if bf.banned? # Banned because of brute force
        @user.errors.add :base, t('errors.locked_out', time: bf.time_left_in_words)
      elsif params[:user][:password].blank?
        # Because of quirks with has_secure_password, blank passwords aren't validated automatically
        @user.errors.add :password, :blank
      elsif !@user.authenticate(params[:user][:current_password])
        @user.errors.add :base, t('.error_current_password_mismatch')
        bf.increment!
      else
        params.require(:user).permit(:password, :password_confirmation)
        @user.update(params.require(:user).permit(:password, :password_confirmation))
      end
    else # Updating all other attributes
      @user.update user_update_params
    end

    # Note: checking user.valid? flushes custom errors, so we call it last
    if !(params[:user][:update_password].nil? && params[:user][:update_profile].nil?) && @user.errors.empty? && @user.valid?
      redirect = user_path(current_user.name_lower)
      flash[:success] = !params[:user][:update_password].nil? ? t('.password_updated') : t('.profile_updated')
    end

    render json: json_result(@user, redirect: redirect)
  end

  private

  def user_update_params
    params.require(:user).permit(:email, :profile, :show_nsfw)
  end

  def permission_to_edit
    @user = User.find_by! name_lower: params[:id]
    no_permission_redirect! unless @user.can_edit?(current_user)
  end
end
