class ApplicationController < ActionController::Base
  include ApplicationHelper, SessionsHelper, FormHelper, HtmlHelper
  protect_from_forgery with: :exception

  before_action :sign_in_user_by_cookie
  before_action :track_referer
  before_action :setup_app_vars
  after_action :track_last_action_at

  rescue_from ActiveRecord::RecordNotFound do |exception|
    @error = t('errors.record_not_found', content: controller_name.classify.downcase)
    render template: 'layouts/error', status: 404, formats: [:html]
  end

  def raise_not_found!
    @error = t('errors.not_found')
    render template: 'layouts/error', status: 404, formats: [:html]
  end

  private

  def sign_in_user_by_cookie
    if cookies[:token] && cookies[:user] && !signed_in?
      bf = BruteForce.within_timeout.first_or_initialize ip_address: request.remote_ip
      unless bf.hidden_banned? # Brute force protection for cookie auth
        user = User.includes(:user_auths)
               .where(name_lower: cookies[:user].downcase)
               .where(user_auths: { auth_key: cookies[:token] })

        if user.any? # Success, user found
          sign_in User.find_by name_lower: cookies[:user].downcase
        else # Failed, silently fails
          bf.hidden_increment!
          cookies.delete :token
          cookies.delete :user
        end
      end
    end
  end

  def track_referer
    # Referrer is tracked for voting metrics for a couple of page loads
    session[:referer_count] ||= 0
    begin
      if URI.parse(request.referer).host != URI.parse(request.base_url).host and !request.referer.blank?
        session[:referer_track] = request.referer
        session[:referer_count] = 0
      end
    rescue URI::InvalidURIError
      # Ignore invalid URIs
    end

    if session[:referer_count] > 2
      session[:referer_track] = nil # The referrer isn't significant anymore
    else
      session[:referer_count] += 1
    end
  end

  def setup_app_vars
    @title = t("#{params[:controller]}.#{params[:action]}.title", default: [t("#{params[:controller]}.title", default: ""), ""])
    @mini_url = request.original_url
    @categories = Category.default_by_parent
    session.delete :last_url if session[:last_url]
  end

  def track_last_action_at
    session[:last_action_at] = DateTime.now
  end

  def must_be_signed_in
    return if signed_in?
    if request.get?
      session[:last_url] = request.url
      flash[:info] = t('errors.sign_in_to_continue')
      redirect_to sign_in_path
    else
      # This is a bit of a shot in the dark, but if its not a get request
      # it's likely a form submission where we can error back to the user
      render json: { errors: [t('errors.not_logged_in')], success: false,
                     sign_in_required: true, sign_in_path: sign_in_path }
    end
  end
end
