include ActionView::Helpers::DateHelper

class SessionsController < ApplicationController
  def create
    errors = {}
    user = User.find_by name_lower: params[:session][:name].downcase
    bf = BruteForce.within_timeout.first_or_initialize ip_address: request.remote_ip

    if bf.banned? # Banned because of brute force
      errors[:locked] = t('errors.locked_out', time: bf.time_left_in_words)
    else
      captcha_success, captcha_error = verify_captcha if bf.risky?
      if bf.risky? && !captcha_success # Bad captcha
        errors[:captcha] = captcha_error
      elsif user && user.authenticate(params[:session][:password]) # Success
        sign_in user, params[:session][:remember] == '1'
        flash[:success] = t('.success_message', user: user.name)
      else # Bad credentials
        if user.nil?
          errors[:name] = t('.error_user_not_found')
        else
          errors[:password] = t('.error_invalid_password')
        end
        bf.increment!
      end
    end
    render json: { errors: errors, success: errors.empty?, captcha: bf.score,
                   redirect: session[:last_url] || root_path }
  end

  def destroy
    if signed_in?
      flash[:success] = t('.success_message', user: current_user.name)
      sign_out
    end
    redirect_to root_url
  end
end
