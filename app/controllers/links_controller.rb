class LinksController < ApplicationController
  before_action :must_be_signed_in, only: [:new, :create]

  def index
    if params[:category].nil?
      @links = Link.get_links current_user_id
      render 'unread'
    else
      @category_path = Category.generate_path params[:category], params[:subcategory]
      @category = Category.find_by! path_indexed: @category_path.gsub('-', '')
      @title = @category.name
      @mini_url = @category.path
      @links = Link.get_links current_user_id, @category
    end
  end

  def show
    @link = Link.full(current_user_id).find_by_slug! params[:slug]
    @title = @link.title
    @mini_url = @link.category_path
    @mini_title = @link.category_name
    @category_path = @link.category_path
  end

  def new
    @category_path = Category.generate_path params[:category], params[:subcategory]
  end

  def create
    link = Link.new link_params
    link.ip_address = request.remote_ip
    link.user_id = current_user.id
    redirect = link.comments_url if verify_captcha(link) && link.save
    render json: json_result(link, redirect: redirect)
  end

  private

  def link_params
    params.require(:link).permit(:title, :category_path, :url, :text, :nsfw)
  end
end
