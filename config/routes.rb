Rails.application.routes.draw do
  scope constraints: { format: :html } do

    resources :sessions, only: [:create, :destroy]
    resources :comments
    resources :links,    only: [:index, :show, :new, :create]
    resources :votes,    only: [:create]
    resources :users

    get "/submit" => "links#new"

    get '/sign_in',  to: 'users#new'
    delete '/sign_out', to: 'sessions#destroy'

    get "/:category/submit" => "links#new"
    get "/:category/:subcategory/submit" => "links#new"
    get "/:category/:subcategory/:slug" => "links#show"
    get "/:category/:subcategory" => "links#index"
    get "/:category" => "links#index"
  end
  root 'links#index'
  get '*unmatched_route', to: 'application#raise_not_found!'
end
